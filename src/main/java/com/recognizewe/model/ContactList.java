package com.recognizewe.model;

import java.util.ArrayList;

public class ContactList {
    private ArrayList<Contact> contacts;

    public ContactList() {
        contacts = new ArrayList<>();
        AddressList addressList = new AddressList();
        for (int i = 0; i < 100; i++) {
            contacts.add(getSpoofedContact(i, addressList.getAddressList()));
        }
    }

    private Contact getSpoofedContact(int i, ArrayList<Address> addressList) {
        Contact contact = new Contact();
        contact.setId(i);
        contact.setNameFirst("John");
        contact.setNameLast("Doe");
        contact.setAddress(addressList.get(i).getStreetAddress());
        contact.setCity(addressList.get(i).getCity());
        contact.setState(addressList.get(i).getState());
        contact.setZip(85222);

        return contact;
    }

    public Contact getContact(int id) {
        for (Contact contact : contacts) {
            if (contact.getId() == id) {
                return contact;
            }
        }
        throw new RuntimeException("Unable to locate specified contact");
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public ArrayList<Contact> putContacts(int id) {
        Contact newContact = new Contact();
        newContact.setId(id);
        newContact.setAddress("address");
        newContact.setCity("Citty");
        return contacts;
    }
}
