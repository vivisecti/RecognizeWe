package com.recognizewe.model;

import java.util.StringTokenizer;

public class Address {
    private String streetAddress;
    private String city;
    private String state;

    public Address(String s) {
        StringTokenizer st = new StringTokenizer(s,"|",false);
        streetAddress = st.nextToken();
        city = st.nextToken();
        state = st.nextToken();
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
