package com.recognizewe.model;

import java.util.ArrayList;
import java.util.List;

public class ActivityList {
    List<Activity> activityList;

    public ActivityList() {
        activityList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            activityList.add(createABullshitActivity(i));
        }
    }

    private Activity createABullshitActivity(int i) {
        Activity activity = new Activity();
        activity.setId(i);

        if(i%2==0) {
            activity.setType(ActivityType.MEETUP);
            activity.setMeetupId(i%2+i);
        } else {
            activity.setType(ActivityType.EVENT);
            activity.setEventId(i%1+i);
        }

        return activity;
    }

    public List<Activity> getActivities() {
        return activityList;
    }
}
