package com.recognizewe.model;

/**
 * Created by VIVIsectI on 10/8/2017.
 */
public enum ActivityType {
    EVENT, MEETUP
}
