package com.recognizewe.model;

import java.util.Date;

public class Event {
    private int id;
    private double location_lat;
    private double location_lon;
    private String address;
    private String city;
    private Date timeStart;
    private Date timeEnd;
    private int pointValue;
    private String description;
    private String name;
    private String stateAbbr;

    public void setId(int id) {
        this.id = id;
    }

    public void setLocation_lon(double location_lon) {
        this.location_lon = location_lon;
    }

    public void setLocation_lat(double location_lat) {
        this.location_lat = location_lat;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public void setPointValue(int pointValue) {
        this.pointValue = pointValue;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public double getLocation_lat() {
        return location_lat;
    }

    public double getLocation_lon() {
        return location_lon;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getStateAbbr() {
        return stateAbbr;
    }

    public Date getTimeStart() {
        return timeStart;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public int getPointValue() {
        return pointValue;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }
}
