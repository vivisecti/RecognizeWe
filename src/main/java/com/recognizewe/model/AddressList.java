package com.recognizewe.model;

import java.util.ArrayList;

public class AddressList {
    private ArrayList<Address> addressList;
    AddressList() {
        addressList = new ArrayList<>();
        addressList.add(new Address("8971 Tailwater Drive|Norristown|PA"));
        addressList.add(new Address("7257 Glenridge Street|Cherry Hill|NJ"));
        addressList.add(new Address("9 North Smith Lane|Lansdowne|PA"));
        addressList.add(new Address("171 Gartner Dr.|Banning|CA"));
        addressList.add(new Address("764 SE. Branch St.|Terre Haute|IN"));
        addressList.add(new Address("61 E. Rockland Street|Canonsburg|PA"));
        addressList.add(new Address("9623 N. Rosewood Dr.|Hazleton|PA"));
        addressList.add(new Address("418 Beech Rd.|Depew|NY"));
        addressList.add(new Address("274 Indian Spring St.|Dallas|GA"));
        addressList.add(new Address("141 West Rosewood St.|Goshen|IN"));
        addressList.add(new Address("7 Grand St.|Orland Park|IL"));
        addressList.add(new Address("70 West Myers Rd.|Anchorage|AK"));
        addressList.add(new Address("9749 Sheffield Drive|Michigan City|IN"));
        addressList.add(new Address("18 Rockledge Street|Bridgeport|CT"));
        addressList.add(new Address("35 Cypress St.|Milwaukee|WI"));
        addressList.add(new Address("381 Hall Ave.|Reisterstown|MD"));
        addressList.add(new Address("954 N. Rockcrest Drive|Auburn|NY"));
        addressList.add(new Address("437 Cedar Avenue|Gurnee|IL"));
        addressList.add(new Address("944 Myers Road|Clarksville|TN"));
        addressList.add(new Address("16 Aspen St.|Holbrook|NY"));
        addressList.add(new Address("608 Hilltop Ave.|Rock Hill|SC"));
        addressList.add(new Address("7403 Riverview Drive|Perkasie|PA"));
        addressList.add(new Address("9808 Proctor Drive|Winter Garden|FL"));
        addressList.add(new Address("109 Nichols Drive|Key West|FL"));
        addressList.add(new Address("8901 Marvon Street|Fresh Meadows|NY"));
        addressList.add(new Address("365 Cedar Dr.|Eau Claire|WI"));
        addressList.add(new Address("345 Branch Ave.|Yorktown|VA"));
        addressList.add(new Address("501 Canal Ave.|Tonawanda|NY"));
        addressList.add(new Address("8875 Belmont Dr.|Sheboygan|WI"));
        addressList.add(new Address("265 North Lantern Rd.|Latrobe|PA"));
        addressList.add(new Address("7861 Lakeshore Rd.|Watertown|MA"));
        addressList.add(new Address("17 Oklahoma Lane|Santa Cruz|CA"));
        addressList.add(new Address("8728 Williams Rd.|Wheeling|WV"));
        addressList.add(new Address("8689 Lakewood St.|Fredericksburg|VA"));
        addressList.add(new Address("8507 South Division Dr.|Patchogue|NY"));
        addressList.add(new Address("15 Peachtree Street|Fairborn|OH"));
        addressList.add(new Address("8680 South Airport Drive|Sun City|AZ"));
        addressList.add(new Address("59 Whitemarsh St.|Sugar Land|TX"));
        addressList.add(new Address("8214 Charles St.|Plattsburgh|NY"));
        addressList.add(new Address("471 Edgewood Road|Alpharetta|GA"));
        addressList.add(new Address("520 Inverness Street|Sarasota|FL"));
        addressList.add(new Address("890 School Street|Lockport|NY"));
        addressList.add(new Address("9157 Maiden Street|Beverly|MA"));
        addressList.add(new Address("470 Leatherwood St.|Fresh Meadows|NY"));
        addressList.add(new Address("7028 Annadale Rd.|Springboro|OH"));
        addressList.add(new Address("57 Oak Meadow Street|Oakland Gardens|NY"));
        addressList.add(new Address("82 Glen Ridge Dr.|Perth Amboy|NJ"));
        addressList.add(new Address("8194 Prince Court|Greenville|NC"));
        addressList.add(new Address("4 Taylor Ave.|Davison|MI"));
        addressList.add(new Address("795 Old Fifth Lane|Jonesborough|TN"));
        addressList.add(new Address("47 Trenton St.|Park Forest|IL"));
        addressList.add(new Address("992 Canal Street|New York|NY"));
        addressList.add(new Address("9488 Brewery Ave.|Vicksburg|MS"));
        addressList.add(new Address("81 Summer Lane|Forney|TX"));
        addressList.add(new Address("9643 Wintergreen Lane|Orland Park|IL"));
        addressList.add(new Address("50 N. Cemetery Street|Chillicothe|OH"));
        addressList.add(new Address("8486 Oxford St.|Hastings|MN"));
        addressList.add(new Address("8265 North Euclid St.|Port Washington|NY"));
        addressList.add(new Address("48 Bridle Street|Highland Park|IL"));
        addressList.add(new Address("212 Pawnee Road|Stuart|FL"));
        addressList.add(new Address("2 Glendale St.|Pembroke Pines|FL"));
        addressList.add(new Address("574 Valley Street|Saint Albans|NY"));
        addressList.add(new Address("420 Willow Road|Oak Forest|IL"));
        addressList.add(new Address("61 North 4th Ave.|Franklin Square|NY"));
        addressList.add(new Address("768 Goldfield St.|Marietta|GA"));
        addressList.add(new Address("6 W. Washington Ave.|Levittown|NY"));
        addressList.add(new Address("943 Goldfield Ave.|Depew|NY"));
        addressList.add(new Address("72 E. Sutor Ave.|Merrick|NY"));
        addressList.add(new Address("922 Ivy St.|Manitowoc|WI"));
        addressList.add(new Address("7182 N. Coffee Avenue|Leland|NC"));
        addressList.add(new Address("7084 Rockville Ave.|Niceville|FL"));
        addressList.add(new Address("68 Durham Ave.|Greensboro|NC"));
        addressList.add(new Address("216 Rockville Court|Randolph|MA"));
        addressList.add(new Address("90 Proctor Rd.|Lagrange|GA"));
        addressList.add(new Address("927 Wild Horse Dr.|Taylors|SC"));
        addressList.add(new Address("875 Smith St.|Lexington|NC"));
        addressList.add(new Address("5 Hudson Road|Ashland|OH"));
        addressList.add(new Address("660 Academy Ave.|Olympia|WA"));
        addressList.add(new Address("8371 Railroad Road|Harlingen|TX"));
        addressList.add(new Address("434 Walnut Ave.|Eastlake|OH"));
        addressList.add(new Address("3 North Bowman Lane|Danville|VA"));
        addressList.add(new Address("80 Fordham St.|Moncks Corner|SC"));
        addressList.add(new Address("963 Spring Dr.|Tucson|AZ"));
        addressList.add(new Address("7476 North Green Hill Ave.|Satellite Beach|FL"));
        addressList.add(new Address("64 Snake Hill Ave.|Newington|CT"));
        addressList.add(new Address("84 Riverview Ave.|Woodside|NY"));
        addressList.add(new Address("24 East Coffee St.|Batavia|OH"));
        addressList.add(new Address("881 Lantern Court|Linden|NJ"));
        addressList.add(new Address("8651 Bridle Drive|Crawfordsville|IN"));
        addressList.add(new Address("7772 Cherry Hill Rd.|Peabody|MA"));
        addressList.add(new Address("7531 Tarkiln Hill Street|Southfield|MI"));
        addressList.add(new Address("9461 Primrose Lane|Doylestown|PA"));
        addressList.add(new Address("9543 Marconi St.|Omaha|NE"));
        addressList.add(new Address("57 Selby St.|Loveland|OH"));
        addressList.add(new Address("8346 Ashley Street|Hackensack|NJ"));
        addressList.add(new Address("9686 Lafayette Street|Attleboro|MA"));
        addressList.add(new Address("3 Talbot Circle|Potomac|MD"));
        addressList.add(new Address("935 Wakehurst Lane|Hyattsville|MD"));
        addressList.add(new Address("9896 North East Road|Moses Lake|WA"));
        addressList.add(new Address("172 San Juan Ave.|Warner Robins|GA"));
    }

    public ArrayList<Address> getAddressList() {
        return addressList;
    }
}
