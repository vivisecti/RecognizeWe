package com.recognizewe.model;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EventList {
    private ArrayList<Event> eventList;

    public EventList() {
        this.eventList = new ArrayList<Event>();
        //todo mocked event list
        AddressList addressList = new AddressList();
        for (int i = 0; i < 20; i++) {
            eventList.add(getSpoofedEvent(i, addressList.getAddressList()));
        }
    }

    public Event getEvent(int i) {
        for (Event event : eventList) {
            if (event.getId() == i) {
                return event;
            }
        }
        throw new RuntimeException("Invalid Event Id Requested");
    }

    private Event getSpoofedEvent(int i, ArrayList<Address> addressList) {
        Event event = new Event();
        event.setId(i);
        event.setLocation_lon(Math.random() * Math.PI * 2);
        event.setLocation_lat(Math.acos(Math.random() * 2 - 1));
        event.setAddress(addressList.get(i).getStreetAddress());
        event.setCity(addressList.get(i).getCity());
        event.setStateAbbr(addressList.get(i).getState());
        Calendar cal = Calendar.getInstance();
        cal.setTime(cal.getTime());
        cal.add(Calendar.DATE, i);
        event.setTimeStart(cal.getTime());
        event.setTimeEnd(cal.getTime());
        event.setPointValue(16);
        event.setDescription("This is going to be an amazing event where we help all kinds of people");
        event.setName("Helping People Together");
        return event;
    }

    public ArrayList<Event> getEvents() {
        return eventList;
    }
}