package com.recognizewe.model;

import java.util.ArrayList;

public class UserList {
    ArrayList<User> userList;

    public UserList() {
        this.userList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            userList.add(generateSpooofedUser(i));
        }
    }

    private User generateSpooofedUser(int i) {
        User user = new User();
        user.setId(i);
        user.setUserName("Bobby McFancypants");
        user.setPointTotal(0);
        return user;
    }

    public User getUser(int id) {
        for (User user : userList) {
            if (user.getId() == id)
                return user;
        }
        throw new RuntimeException("Invalid userId Specified");
    }
}
