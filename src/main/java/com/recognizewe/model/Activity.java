package com.recognizewe.model;

public class Activity {
    int id;
    int eventId;
    ActivityType type;
    int meetupId;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public ActivityType getType() {
        return type;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }

    public int getEventID() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;

    }

    public int getMeetupId() {
        return meetupId;
    }

    public void setMeetupId(int meetupId) {
        this.meetupId = meetupId;

    }
}