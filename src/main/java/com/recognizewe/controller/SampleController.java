package com.recognizewe.controller;

import com.recognizewe.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class SampleController {

    @RequestMapping("/")
    public String loadHomePage(Model m) {
        m.addAttribute("message", "Hello World!");
        return "index";
    }

    @RequestMapping(value = "/contacts/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Contact getContact(@PathVariable("id") int id) {
        return new ContactList().getContact(id);
    }

    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<Contact> getContacts() {
        return new ContactList().getContacts();
    }

    @RequestMapping(value = "/contacts", method = RequestMethod.PUT)
    @ResponseBody
    public ArrayList<Contact> putContacts(@PathVariable("id") int id) {
        return new ContactList().putContacts(id);
    }


    @RequestMapping(value = "/events", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<Event> getEvents() {
        return new EventList().getEvents();
    }

    @RequestMapping(value = "/events/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Event getEvent(@PathVariable("id") int id) {
        return new EventList().getEvent(id);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User getUser(@PathVariable("id") int id) {
     Activity activity;
        return new UserList().getUser(id);
    }

    @RequestMapping(value = "/activities", method = RequestMethod.GET)
    @ResponseBody
    public List<Activity> getUser() {
        return new ActivityList().getActivities();
    }



}