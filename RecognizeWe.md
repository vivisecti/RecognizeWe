﻿
#RecognizeWe
RecognizeWe is an app that allows organizations to connect with and reward volunteers for their hard work. Unlike some volunteer reward programs that use hours to calculate points, RecongnizeWe use a task system that can be customized to the organization's specific needs. Therefore the points assigned to the task can reflect the task's impact on the community and give volunteers and organizations a more concrete way to measure impact. 

> Written with [StackEdit](https://stackedit.io/).